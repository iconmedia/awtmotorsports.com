<cfmodule template="/modules/layout.cfm"
	title="Contact - #Application.TITLE#"
	page="contact"
    keywords=""
	description=""
	metaTitle="">

<div id="thanks">
	<!---<div class="parallax">
        <div class="img" style="background-image: url(/images/contact-bg.jpg);"></div>
        <h1 class="padded text-uppercase white">Thank You</h1>
    </div>--->
    <cfoutput>
        <div class="parallax">
            <div class="img bg-top" style="background-image: url('/images/header-bg.jpg');"></div>
            <div class="row content-header">
                <div class="column medium-6">
                    <h1 class="text-uppercase white">Thank you</h1>
                </div>
                <div class="column medium-6">
                    <h5>#Application.TITLE2#</h5>
                    <h2>#Application.settings.vchrPhoneNumber#</h2>
                </div>
            </div>
        </div>
    </cfoutput>
    
	<div class="padded row text-center">
		<div class="padded small-12 columns">
	        <h3 class="text-uppercase">Your message has been submitted.</h3>
	        <cfoutput>
	            <p>
                    An #Application.TITLE2# representative will contact you in a timely manner.
                </p>
	        </cfoutput>
	    </div>
	</div>
</div>

</cfmodule>
