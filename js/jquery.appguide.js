/* global ICAPA */
var intYear = 0;
var strMakeAP = "";
var intMake = 0;
var strModelAP = "";
var intModel = 0;
var strSubModelOpt = "";
var intSubModel = "";
var strSizeText = "";
var lastsize = "";
var intSizeID = 0;
var url = "";
var intID = 7;
var intConfigID = 2133;
var ajaxPage = false;

ICAPA.getJSON("ap-years.cfm?configID=" + intConfigID);

var ICAPP = function ICAPP() {
    var initChange = function initChange() {
        $("#year").change(function() {
            intYear = $(this).val();
            if (intYear !== "") {
                var url = "ap-makes.cfm?yearid=" + intYear + "&configID=" + intConfigID + "&output=json&callback=makeObj";
                intMake = 0;
                intModel = 0;
                intSubModel = 0;
                ICAPA.getJSON(url);
                setSession(intYear, "year", $("#year").html());
                $("#stryear").html("");
                $("#strmake").html("");
                $("#strmodel").html("");
                $("#strsubmodelopt").html("");
                $("#strsizetext").html("");
            } else {
                $("#make").attr("disabled", "disabled");
                $("#model").attr("disabled", "disabled");
                $("#submodel").attr("disabled", "disabled");
            }
        });

        $("#make").change(function() {
            intMake = $(this).val();
            strMakeAP = $(this).find("option:selected").text();
            if (intMake !== "") {
                var url = "ap-models.cfm?yearid=" + intYear + "&makeid=" + intMake + "&configID=" + intConfigID + "&output=json&callback=modelObj";
                intModel = 0;
                intSubModel = 0;
                ICAPA.getJSON(url);
                setSession(intMake, "make", $("#make").html(), strMakeAP);
                $("#strmake").html("");
                $("#strmodel").html("");
                $("#strsubmodelopt").html("");
                $("#strsizetext").html("");
            } else {
                $("#model").attr("disabled", "disabled");
                $("#submodel").attr("disabled", "disabled");
            }
        });

        $("#model").change(function() {
            intModel = $(this).val();
            strModelAP = $(this).find("option:selected").text();
            if (intModel !== "") {
                var url = "ap-submodels.cfm?yearid=" + intYear + "&makeid=" + intMake + "&modelid=" + intModel + "&output=json&callback=submodelObj";
                intSubModel = 0;
                ICAPA.getJSON(url);
                setSession(intModel, "model", $("#model").html(), strModelAP);
                $("#strmodel").html("");
                $("#strsubmodelopt").html("");
                $("#strsizetext").html("");
            } else {
                $("#submodel").attr("disabled", "disabled");
            }
        });

        $("#submodel").change(function() {
            intSubModel = $(this).val();
            strSubModelOpt = $(this).find("option:selected").text();
            if (intSubModel !== "") {
                url = "ap-sizes.cfm?yearid=" + intYear + "&makeid=" + intMake + "&modelid=" + intModel + "&submodel=" + intSubModel + "&id=" + intID + "&configId=" + intConfigID;
                ICAPA.getJSON(url);
                setSession(intSubModel, "submodel", $("#submodel").html(), strSubModelOpt);
                $("#strsubmodelopt").html("");
                $("#strsizetext").html("");
            }
        });

        $("#size").change(function() {
            var intIndex = $(this).val();
            strSizeText = $(this).find("option:selected").text();
            intSizeID = intIndex;
            if (intIndex !== "") {
                lastsize = intIndex;
                $.magnificPopup.open({
                    items: {
                        src: "<div class=\"white-popup processing\"><i class=\"fa fa-spinner fa-spin fa-4x\"></i></div>"
                    },
                    type: "inline",
                    modal: true
                });
                setSession(intIndex, "size", $("#size").html(), strSizeText);
            } else {
                $("#size option[value=" + lastsize + "]").attr("selected", "selected");
            }
        });
    };

    var setSession = function setSession(val, type, boxVal, text) {
        var dataString = "funct=" + type + "&" + type + "=" + val + "&box=" + boxVal + "&text=" + text;
        $.ajax({
            type: "POST",
            url: "/ajax/createSession.cfm",
            data: dataString,
            success: function(data) {
                if (data == "expired")
                    window.location.reload();
                else {
                    if (type == "size") {
                        if (!ajaxPage) {
                            window.location.href = "/wheels/?fit=1&sizeID=" + intSizeID;
                        }
                    } else if (type == "clear") {
                        window.location.href = "/wheels/";
                    }
                }
            }
        });
    };

    return {
        initChange: initChange,
        setSession: setSession
    };
}();
