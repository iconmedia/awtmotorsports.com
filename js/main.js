/* global $, Foundation, MotionUI, ICAPP, List */
/* ==========================================================================
   Table of Contents
   ========================================================================== */


/*
    1.   General
    ---  Product Pages  ---
    2.1  Brand Pages
    2.2  Detail Pages
    ---  Gallery Pages  ---
    3.1  Gallery Landing Page
    3.3  Gallery Detail Page
    ---  Other Pages  ---
    4.1  Home Page
    4.2  Contact Page
    4.3  Article Page
*/
$(function ultraApp() {
  /* ==========================================================================
     1. General
     ========================================================================== */
  // Cache
  var $img = $('.parallax .img');
  var $heading = $('.parallax h1');
  var scroller;
  var wh = $(window).height();
  var intBrand = 0;
  var intWheel = 0;
  var intFinish = '';
  var decDiam = '0.0';
  var decWidth = '0.0';
  var strLug = '';
  var strPCD = '';
  var intOffStart = -130;
  var intOffEnd = 130;
  var intOffStartSel = -140;
  var intOstart = 0;
  var arrReturn = [];
  var strOffResult = '';
  var inventoryOptions;

  // Newsletter Validation
  $('form.ajax-form').submit(function formRequired(event) {
    var $this = $(this);
    var $requiredFields = $this.find('input[required]');
    var fail = false;
    var message = '';

    event.preventDefault();
    $requiredFields.each(function checkRequired(index, el) {
      if ($(el).val() === '') {
        fail = true;
        message = $(el).attr('data-error');
      }
    });
    if (fail === true) {
      $('#newsletterModal .lead').text(message);
      $('#newsletterModal').foundation('open');
      return false;
    }
    // Submit via AJAX
    $.post($this.attr('action'), $this.serialize(), function submitForm(data) {
      $('#newsletterModal .lead').text(data);
      $('#newsletterModal').foundation('open');
    });
    return true;
  });


  $('.fitment-open').magnificPopup({
    items: {
      src: '#fitment-popup',
      type: 'inline'
    }
  });

  $('#fitmentSubmit').on('click', function submitFitment() {
    var size = $('#size');
    if (size.val() && size.val() !== 'Select Size') {
      size.change();
    }
  });

  // Mobile Accordion
  if ($('.accordion').length && Foundation.MediaQuery.atLeast('medium')) {
    $('.accordion').each(function openAccordion() {
      $(this).foundation('down', $(this).find('.accordion-content').first(), true);
    });
  }

  // Scroll to section
  $('.scroll-to').on('click', function scrollToElem(event) {
    var target = $($(this).attr('href'));
    if (target.length) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });

  // update header menu
  $(window).scroll(function () {
    console.log($(window).width());
    if ($(window).width() >= 1009) {
      //console.log('bloop bleep bleep beep');
      if ($(this).scrollTop() > 50) {
        $("header").addClass("not-transparent").fadeIn(2000);
      } else {
        $("header").removeClass("not-transparent");
      }
      if ($(this).scrollTop() > 50) {
        $("header.int-header").addClass("not-transparent").fadeIn(2000);
      } else {
        $("header.int-header").removeClass("not-transparent");
      }
    }
  });

  // Varify captcha
  $('.rfqSubmit, .check-captcha').on('click', function () {
    if (grecaptcha.getResponse() == "") {
      var response = document.getElementById('check-captcha');
      response.innerHTML = 'Captcha box not selected';
      return false;
    } else {
      return true;
    }
  });

  /* ==========================================================================
     2.1 Brand Pages
     ========================================================================== */
  // PARALLAX EFFECT   http://codepen.io/sallar/pen/lobfp
  // requestAnimationFrame Shim
  window.requestAnimFrame = (function animFramPoly() {
    return window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function animTimout(callback) {
        window.setTimeout(callback, 1000 / 60);
      };
  }());

  // Scroller
  function Scroller() {
    this.latestKnownScrollY = 0;
    this.ticking = false;
  }

  Scroller.prototype = {
    // Initialize
    init: function init() {
      window.addEventListener('scroll', this.onScroll.bind(this), false);
    },

    // Capture Scroll
    onScroll: function onScroll() {
      this.latestKnownScrollY = window.scrollY;
      this.requestTick();
    },

    // Request a Tick
    requestTick: function requestTick() {
      if (!this.ticking) {
        window.requestAnimFrame(this.update.bind(this));
      }
      this.ticking = true;
    },

    // Update
    update: function update() {
      var currentScrollY = this.latestKnownScrollY;

      // Do The Dirty Work Here
      var imgScroll = currentScrollY / 2;
      var headScroll = currentScrollY / 3;

      this.ticking = false;

      $img.css({
        'transform': 'translateY(' + imgScroll + 'px)',
        '-moz-transform': 'translateY(' + imgScroll + 'px)',
        '-webkit-transform': 'translateY(' + imgScroll + 'px)'
      });

      $heading.css({
        'transform': 'translateY(' + headScroll + 'px)',
        '-moz-transform': 'translateY(' + headScroll + 'px)',
        '-webkit-transform': 'translateY(' + headScroll + 'px)'
      });
    }
  };

  // Attach!
  if ($('.parallax .img').length || $('.parallax h1').length) {
    scroller = new Scroller();
    scroller.init();
  }

  // Filter Categories
  $('.brand-nav').change(function changeBrand() {
    window.location.replace($(this).val());
  });

  // Quick View Popup
  $('.quickview-open').click(function () {
    var $this = $(this);
    var finishName = $this.data('finishname');
    var wheelName = $this.data('wheelname');
    var brandName = $this.data('brandname');
    var wheelImage = $this.data('wheelimage');
    var wheelLink = $this.data('wheellink');
    var wheelSizes = $this.data('sizes');
    console.log(wheelSizes);
    $('#quickview-popup')
      .find('.popup-brandname')
      .html(brandName);
    $('#quickview-popup')
      .find('.popup-wheelname')
      .html(wheelName);
    $('#quickview-popup')
      .find('.popup-wheelfinish')
      .html(finishName);
    $('#quickview-popup')
      .find('.popup-sizes')
      .html(wheelSizes);
    $('#quickview-popup')
      .find('.popup-wheelimage')
      .attr("src", wheelImage);
    $('#quickview-popup')
      .find('.popup-wheellink')
      .attr("href", wheelLink);


    $.magnificPopup.open({
      items: {
        src: '#quickview-popup',
        type: 'inline'
      }
    });



  });
  // Header Filters
  /*
    $('#filter-brand').change(function changeMake() {
      $('#wheel-search form').submit();
    });
  $('#filter-finish').change(function changeMake() {
      $('#wheel-search form').submit();
    });
  $('#filter-bolt').change(function changeMake() {
      $('#wheel-search form').submit();
    });
  $('#filter-size').change(function changeMake() {
      $('#wheel-search form').submit();
    });
  $('#filter-offset').change(function changeMake() {
      $('#wheel-search form').submit();
    });
  */

  $('#filter-size').change(function changeMake() {
    $('#wheel-search form').submit();
  });
  $('#filter-finish').change(function changeMake() {
    $('#wheel-search form').submit();
  });

  /* ==========================================================================
     2.2 Detail Pages
     ========================================================================== */
  // Slider and popup for main image
  function initializeMainImage() {
    $('.main-image').magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1]
      }
    }).slick({
      arrows: false,
      asNavFor: '.alt-images',
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });

    $('.alt-images').slick({
      arrows: false,
      asNavFor: '.main-image',
      dots: true,
      focusOnSelect: true,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1
    });
  }

  initializeMainImage();

  // Get see on vehicle image
  function detailCarImage(car, year, make, model, submodel, part, color, body, configid, option) {
    var colorIds;
    var colorNames = [];
    var bodyNames = [];
    var h;
    var j;

    try {
      $.getJSON('//ver1.iconfigurators.com/ap-json/ap-image-part.aspx?year=' + year + '&make=' + make + '&model=' + model + '&submodel=' + submodel + '&part=' + part + '&color=' + color + '&body=' + body + '&ID=' + configid + '&option=' + option, function getCarImage(data) {
        if (data.Result > 0) {
          colorIds = data.img[0].colorID;
          colorNames = [];
          $.each(data.img[0].colorName, function checkDuplicates(i, el) {
            if ($.inArray(el, colorNames) === -1) {
              colorNames.push(el);
            }
          });

          car.find('img').attr('src', data.img[0].src);
          $('#vehicle-colors').html('');

          for (h = 0; h < colorIds.length; h++) {
            if (colorIds[h] == color) {
              $('#vehicle-colors').append('<option value="' + colorIds[h] + '" selected>' +
                colorNames[h] + '</option>');
            } else {
              $('#vehicle-colors').append('<option value="' + colorIds[h] + '" >' +
                colorNames[h] + '</option>');
            }
          }

          // Create body style select
          $('#vehicle-body').html('');
          bodyNames = [];
          $.each(data.img[0].BodyStyle, function checkDuplicates(i, el) {
            if ($.inArray(el, bodyNames) === -1) {
              bodyNames.push(el);
            }
          });

          for (j = 0; j < bodyNames.length; j++) {
            if (bodyNames[j] == body) {
              $('#vehicle-body').append('<option data-id="' + j + '" value ="' + bodyNames[j] +
                '" selected>' + bodyNames[j] + '</option>');
            } else {
              $('#vehicle-body').append('<option data-id="' + j + '" value ="' + bodyNames[j] +
                '" >' + bodyNames[j] + '</option>');
            }
          }

          $('#see-on-vehicle-detail > a').attr('href', function replaceUrl(k, a) {
            return a.replace(/(&body=)([0-9]+)?(&color=)([0-9]+)?/ig, '$1' +
              $('#vehicle-body').find(':selected').data('id') + '$3' + $('#vehicle-colors').val());
          });
        } else {
          car.find('.img-wrapper').addClass('no-fitment')
            .find('.error').text('We do not currently have an image of this ' +
              'wheel on your vehicle. Check back soon.');
        }
      });
    } catch (err) {
      car.find('.img-wrapper').addClass('no-fitment')
        .find('.error').text('Error Loading Image for Your Vehicle');
    }
  }

  // Set Dropdowns
  function setDropdownHandlers(car, year, make, model, submodel, configid, option) {
    var body = '';
    var color = 0;
    var part = '';
    // Bind the color select
    $('#vehicle-colors').change(function changeColor() {
      body = $('#vehicle-body').val();
      color = $('#vehicle-colors').val();
      part = encodeURIComponent($('#see-on-vehicle-detail').attr('data-part'));
      $('#see-on-vehicle-detail > a').attr('href', function replaceUrl(i, a) {
        return a.replace(/(&color=)([0-9]+)?/ig, '$1' + color);
      });
      detailCarImage(car, year, make, model, submodel, part, color, body, configid, option);
    });
    // Bind the body select
    $('#vehicle-body').change(function changeBody() {
      body = $('#vehicle-body').val();
      color = 0;
      part = encodeURIComponent($('#see-on-vehicle-detail').attr('data-part'));
      $('#see-on-vehicle-detail > a').attr('href', function replaceUrl(i, a) {
        return a.replace(/(&body=)([0-9]+)?(&color=)([0-9]+)?/ig, '$1' +
          $('#vehicle-body').find(':selected').data('id') + '$3');
      });
      detailCarImage(car, year, make, model, submodel, part, color, body, configid, option);
    });
  }

  function setVehicle() {
    var car = $('#see-on-vehicle-detail');
    var year = car.data('year');
    var make = car.data('make');
    var model = car.data('model');
    var submodel = car.data('submodel');
    var part = encodeURIComponent(car.attr('data-part'));
    var configid = car.data('config');
    var option = car.data('option');
    if (typeof option === 'undefined') {
      option = '';
    }
    detailCarImage(car, year, make, model, submodel, part, 0, '', configid, option);
    setDropdownHandlers(car, year, make, model, submodel, configid, option);
  }
  // Check if vehicle is set in session first
  if ($('#see-on-vehicle-detail').length) {
    setVehicle();
  }

  // Click on finish images
  $('.detail-thumbs a').click(function changeFinish(e) {
    var $this = $(this);
    var car = '';
    var year = '';
    var make = '';
    var model = '';
    var submodel = '';
    var part = '';
    var configid = '';
    var option = '';
    var color = '';
    var body = '';
    e.preventDefault();

    // Update main image with new finish
    $.get('/ajax/getFinishImages.cfm?id=' + $this.data('id'), function getNewImages(data) {
      $('#detail-top .images').html(data);
      initializeMainImage();
    });
    $('#finish').text($this.find('.finish-name').text() + ' - ' + $this.find('.lugs').text());

    // Replace see on image vehicle
    if ($('#see-on-vehicle-detail').length) {
      car = $('#see-on-vehicle-detail');
      car.attr('data-part', $this.attr('data-part'));
      year = car.data('year');
      make = car.data('make');
      model = car.data('model');
      submodel = car.data('submodel');
      part = encodeURIComponent($this.attr('data-part'));
      configid = car.data('config');
      color = $('#vehicle-colors').val();
      body = $('#vehicle-body').val();
      if (typeof car.data('option') !== 'undefined') {
        option = car.data('option');
      }

      car.children('a').attr('href', function replaceUrl(i, a) {
        return a.replace(/(wheelimage=)([0-9]+)?/ig, '$1' + $this.data('id'));
      });

      if (!$this.attr('data-part') || $this.data('lugs') !== car.data('lugs')) {
        car.find('.img-wrapper').addClass('no-fitment')
          .find('.error').text('Current finish/lug does not fit selected vehicle');
      } else {
        detailCarImage(car, year, make, model, submodel, part, color, body, configid, option);
        car.find('.img-wrapper').removeClass('no-fitment');
      }

      setDropdownHandlers(car, year, make, model, submodel, configid, option);
    }

    if (!Foundation.MediaQuery.atLeast('medium')) {
      $('html, body').animate({
        scrollTop: $('.main-image').offset().top - $('header.fixed').height()
      }, 1000);
    }
  });

  // Wheel gallery images
  $('.wheel-gallery-photo').magnificPopup({
    type: 'image',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      markup: '<div class="wheel-gallery-figure mfp-figure">' +
        '<div class="mfp-close"></div>' +
        '<div class="mfp-img"></div>' +
        '<div class="mfp-bottom-bar">' +
        '<div class="mfp-title"></div>' +
        '<div class="mfp-counter"></div>' +
        '</div>' +
        '</div>',
      titleSrc: 'data-html'
    }
  });

  // Technical Specifications specs
  /*$('tab-title').click(function(e) {
      e.preventDefault();
      $(this).addclass("active");;
  });*/

  // Technical Specifications specs
  /*$('.tab-title').click(function(e) {
      e.preventDefault();
      $(".tabs .active").removeClass("active");
      $(this).addClass("active");
  });*/

  // Technical Specification specs tab
  // $('[data-tabs]').eq(0).foundation('selectTab', $('#spec-tabs'));

  //RFQ Popup 
  $('.open-quote-popup').magnificPopup({
    items: {
      src: '#quote-popup',
      type: 'inline'
    }
  });

  // Show/Hide excess information
  $(document).ready(function () {
    if ($(".wheel-galleryImages > li").length > 6) {
      $(".wheel-galleryShowButton").removeClass("hidden");
    }
    if (getRows(".wheel-paragraph--limit") > 5) {
      $('.wheel-paragraph--limit').addClass('wheel-paragraph--height');
      $(".wheel-paragraphShowButton").removeClass("hidden");
    }
    if (getRows(".wheel-size--limit") > 5) {
      $('.wheel-size--limit').addClass('wheel-size--height');
      $(".wheel-sizeShowButton").removeClass("hidden");
    }
  });

  // Show/Hide excess paragraph lines
  function getRows(selector) {
    var height = $(selector).height();
    var line_height = $(selector).css('line-height');
    height = parseFloat(height);
    line_height = parseFloat(line_height);
    var rows = height / line_height;
    return Math.round(rows);
  }

  function toggleButtons(classSelect, cssSelect, showBtnToggle, hideBtnToggle) {
    $(classSelect).toggleClass(cssSelect);
    $(showBtnToggle).toggleClass('hidden');
    $(hideBtnToggle).toggleClass('hidden');
  }

  $('.wheel-paragraphShowButton, .wheel-paragraphHideButton').on('click', function () {
    toggleButtons('.wheel-paragraph--limit', 'wheel-paragraph--height', '.wheel-paragraphShowButton', '.wheel-paragraphHideButton');
  });

  $('.wheel-galleryShowButton, .wheel-galleryHideButton').on('click', function () {
    toggleButtons('.wheel-galleryImages > li', 'wheel-galleryList', '.wheel-galleryShowButton', '.wheel-galleryHideButton');
  });
  $('.wheel-sizeShowButton, .wheel-sizeHideButton').on('click', function () {
    toggleButtons('.wheel-size--limit', 'wheel-size--height', '.wheel-sizeShowButton', '.wheel-sizeHideButton');
  });
  /* ==========================================================================
     3.1  Gallery Landing Page
     ========================================================================== */
  $('#gallery a.video').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });
  
  
  // Gallery filter - it's not working on the modules for some reason....
  $('#gallery-make').change(function changeMake() {
      $('#gallery-model').val('');
      $('#gallery-brand').val('');
      $('#gallery-search form').submit();
  });

  $('#gallery-model').change(function changeModel() {
      $('#gallery-brand').val('');
      $('#gallery-search form').submit();
  });

  $('#gallery-brand').change(function changeBrand() {
      $('#gallery-search form').submit();
  });


  /* ==========================================================================
     3.2  Gallery Detail Page
     ========================================================================== */
  $('#gallery-main-image').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    }
  });

  $('#gallery-main-image').slick({
    adaptiveHeight: false,
    arrows: false,
    asNavFor: '#gallery-thumbs',
    fade: true,
    lazyLoad: 'ondemand'
  });
  $('#gallery-thumbs').slick({
    arrows: false,
    asNavFor: '#gallery-main-image',
    focusOnSelect: true,
    infinite: false,
    slidesToShow: 5,
    responsive: [{
      breakpoint: 600,
      settings: {
        dots: true,
        slidesToScroll: 3,
        slidesToShow: 3
      }
    }]
  });

  $('.racer #gallery-thumbs').slick('slickSetOption', 'slidesToShow', 6, true);
  $('.racer #gallery-main-image').slick('slickSetOption', 'adaptiveHeight', true, true);

  /* ==========================================================================
     4.1  Home Page
     ========================================================================== */
  /* MOTION UI - FADE IN TRANSITIONS /////////*/
  /*if (Foundation.MediaQuery.atLeast('medium')) {
      var wh = $(window).height();

      $(window).scroll(function checkScroll() {
        $('.animate').each(function animateItem() {
          var $this = $(this);
          if (($this.offset().top - $(window).scrollTop()) - wh <= -350) {
            MotionUI.animateIn($this, 'fade-in', function adjustOpacity() {
              $this.css('opacity', '1');
            });
            $this.removeClass('animate');
          }
        });
      });
    }*/

  /* MOTION UI - RANDOM TRANSITIONS /////////*/
  if (Foundation.MediaQuery.atLeast('medium')) {
    $(window).scroll(function scrollPage() {
      $('.animate').each(function animateElem() {
        var $this = $(this);
        var transitions = ['slide-in-left', 'slide-in-up', 'slide-in-right', 'fade-in',
          'hinge-in-from-right', 'hinge-in-from-bottom', 'hinge-in-from-left'
        ];
        var randomNum = Math.floor(Math.random() * (transitions.length - 1));
        if (($this.offset().top - $(window).scrollTop()) - wh <= 0) {
          MotionUI.animateIn($this, transitions[randomNum]);
          $this.removeClass('animate');
        }
      });
    });
  }

  /* homepage slider */
  $('.hp-hero-slider').slick({
    autoplay: true,
    arrows: false
  });

  $('.hp-slider').slick({
    autoplay: true,
    arrows: true,
    nextArrow: '<i class="fa new-arrow-right fa-chevron-right" aria-hidden="true"></i>',
    prevArrow: '<i class="fa new-arrow-left fa-chevron-left" aria-hidden="true"></i>',
    appendArrows: $("#slider-arrows"),
    /*appendArrows: ($('.hp-slider').hasClass('no-append')) ? '' : $('.slider-dots', '.hp-slider'),*/
    dots: true,
    /*customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data('thumb');
        console.log(thumb);
        return '<a><img src="'+thumb+'"></a>';
    }*/
    customPaging: function (slider, i) {
      return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
    }
  });


  /*    var feed = new Instafeed({
          clientId: "8b4515101f4c4378a8aacd28bcdc194c",
          accessToken: '1287939109.1677ed0.29ece8b4c7ef441684ce8ddf50569b9c',
          get: 'user',
          userId: 1287939109,
          limit: 4,
          resolution: 'low_resolution',
          template: '<div class="column"><a href="{{link}}"><img src="{{image}}" /></a></div>'
      });
      feed.run();*/


  // Instagram widget
  if (jQuery("#instagram").length > 0) {
    var feed = new Instafeed({
      clientID: "1fab031ff0194f80b430c76a0a9e66a7",
      accessToken: "2947847191.1677ed0.eeaa3ac5733743f3a328024c6b0d80cd",
      get: "user",
      userId: 2947847191,
      limit: 10,
      target: 'instafeed',
      resolution: "low_resolution",
      template: '<div class="animate column"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>'
    });
    feed.run();
  }

  /* ==========================================================================
     4.2  Contact Page
     ========================================================================== */
  $('#vchrCountry select').change(function changeCountry() {
    $.get('/ajax/getStates.cfm?country=' + $(this).val(), function getStates(data) {
      $('#vchrState select').html(data).prop('disabled', false);
    });
  });
  $('#form-left form').submit(function checkRequired() {
    var fail = false;
    var message = '';
    var errorCount = 0;
    var name = '';
    var title = '';
    $('#form-left form [required]').each(function checkFields(index, element) {
      var $element = $(element);
      if ($element.val() === '') {
        $element.css('background', 'red');
        fail = true;
        errorCount++;
        name = $element.siblings('label').replace('*', '');
        message += name + ' is required. ';
      }
    });
    if (fail) {
      title = $(this).attr('data-title');
      $('#modal').html('<p>Form submission failed for the following reason(s):' + message + '</p>')
        .dialog({
          minHeight: 150,
          width: 300,
          modal: true,
          title: title,
          closeText: 'X',
          buttons: {
            Okay: function closeModal() {
              $(this).dialog('close');
            }
          }
        });
      return false;
    }
    return true;
  });
  /* ==========================================================================
     4.3  Inventory Page
     ========================================================================== */
  function getData(url, selector) {
    var i;
    $.get('/ajax/getData.cfm?' + url, function getInventory(data) {
      if (selector === '#offstart') {
        arrReturn = data.split(',');

        if (intOstart === 0) {
          intOffStart = isNaN(parseInt(arrReturn[0], 10)) ? -130 : parseInt(arrReturn[0], 10);
        } else {
          intOffStart = intOffStartSel + 10;
        }

        intOffEnd = isNaN(parseInt(arrReturn[1], 10)) ? 130 : parseInt(arrReturn[1], 10);
        strOffResult = '<option value="">All</option>';

        for (i = intOffStart; i <= intOffEnd; i += 10) {
          strOffResult += '<option value="' + i + '">' + i + '</option>';
          if (i >= 130) {
            break;
          }
        }

        if (intOstart === 0) {
          $(selector).html(strOffResult);
        }
        $('#offend').html(strOffResult);
      } else {
        $(selector).html(data);
      }
    });
  }

  if ($('#inventory').length) {
    $('#brand').on('change', function changeBrand() {
      var url = '';
      intBrand = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand;
      getData('step=1' + url, '#wheel');
      getData('step=2' + url, '#finish');
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#wheel').on('change', function changeWheel() {
      var url = '';
      intWheel = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel;
      getData('step=2' + url, '#finish');
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#finish').on('change', function changeFinish() {
      var url = '';
      intFinish = ($(this).val() !== '') ? $(this).val() : '';
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish;
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#diam').on('change', function changeDiam() {
      var url = '';
      decDiam = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
        '&diam=' + decDiam;
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#width').on('change', function changeWidth() {
      var url = '';
      decWidth = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
        '&diam=' + decDiam + '&width=' + decWidth;
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#lugs').on('change', function changeLug() {
      var url = '';
      strLug = ($(this).val() !== '') ? $(this).val() : '';
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
        '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug;
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#pcd').on('change', function changePCD() {
      var url = '';
      strPCD = ($(this).val() !== '') ? $(this).val() : '';
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
        '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug + '&pcd=' + strPCD;
      getData('step=7' + url, '#offstart');
    });

    $('#offstart').on('change', function changeOffstart() {
      var url = '';
      intOffStartSel = ($(this).val() !== '') ? parseInt($(this).val(), 10) : -140;
      intOstart = 1;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
        '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug + '&pcd=' + strPCD +
        '&offSetStart=' + intOffStartSel;
      getData('step=7' + url, '#offstart');
    });
  }

  if ($('#inventory-results').length) {
    inventoryOptions = {
      page: 5000,
      valueNames: ['partNum', 'brand', 'name', 'finish', 'diam', 'width', 'bolt', 'offset', 'qty']
    };
    var userList = new List('inventoryTable', inventoryOptions);

    $('.sort').on('click', function sortItems() {
      var $this = $(this);
      var icon = 'fa-caret-up';
      if ($this.find('i').hasClass('fa-caret-up')) {
        icon = 'fa-caret-down';
      } else {
        icon = 'fa-caret-up';
      }
      $('.sort i').remove();
      $this.append('<i class="fa ' + icon + '"></i>');
    });

    $('.image_link').magnificPopup({
      type: 'image'
    });
  }
});
/* ==========================================================================
   2.2 Accessories Pages
   ========================================================================== */
$('#filter-captype').change(function changeMake() {
  $('#accessory-search form').submit();
});
$('#filter-finish').change(function changeMake() {
  $('#accessory-search form').submit();
});
$('#filter-brand').change(function changeMake() {
  $('#accessory-search form').submit();
});
$('#filter-size').change(function changeMake() {
  $('#accessory-search form').submit();
});
$('#filter-lug').change(function changeMake() {
  $('#accessory-search form').submit();
});
$('#filter-filter').change(function changeMake() {
  $('#accessory-search form').submit();
});

$('.content-popup').magnificPopup({
  type: 'image',
  closeOnContentClick: true
});