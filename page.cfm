<cfquery
    name = "GetPage"
    datasource = "#Application.DSN#">
    SELECT vchrName
        ,vchrMarketingTitle
        ,vchrMarketingKeywords
        ,vchrMarketingDescription
        ,txtDescription
        ,vchrImage
    FROM tbl_content
    WHERE  1 = 1
    <cfif structKeyExists(url, "id")>
    	AND intPageID = <cfqueryparam value = "#url.id#" CFSQLType="cf_sql_integer">
    <cfelse>
    	AND intPageID = <cfqueryparam value = "#session.intPageID#" CFSQLType="cf_sql_integer">
	</cfif>
       AND intAccountID =  <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.settings.intACCOUNTID#">
</cfquery>

<cfmodule template="/modules/layout.cfm"
	title="#GetPage.vchrName# - #Application.TITLE#"
	page="page"
    keywords="#GetPage.vchrMarketingKeywords#"
	description="#GetPage.vchrMarketingDescription#"
	metaTitle="#GetPage.vchrMarketingTitle#">

<div id="page">
    <cfoutput query="GetPage">
        <div class="parallax">
            <!---<div class="img bg-top" style="background-image: url(#Application.SYSTEMIMAGEPATH#content/#vchrImage#);"></div>--->
            <cfif GetPage.vchrImage neq ''>
                <div class="img bg-top" style="background-image: url('#Application.SYSTEMIMAGEPATH#content/#vchrImage#');"></div>
            <cfelse>
                <div class="img bg-top" style="background-image: url('/images/header-bg.jpg');"></div>
            </cfif>
            <div class="row content-header">
                <div class="column medium-6 large-12">
                    <h1 class="text-uppercase white">#vchrName#</h1>
                </div>
                <div class="column medium-6 show-for-medium-only">
                    <h5>#Application.TITLE2#</h5>
                    <h2>#Application.settings.vchrPhoneNumber#</h2>
                </div>
            </div>
            
        </div>
    </cfoutput>
    <div class="page-content">
        <div class="row">
            <cfoutput query="GetPage">
                <div class="small-12 columns">
                    #txtDescription#
                </div>
            </cfoutput>
        </div>
    </div>
</div>
</cfmodule>
