<div id="gallery-header">
    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
        <li class="accordion-item">
            <a href="#gallery-search" role="tab" class="show-for-small-only accordion-title" id="gallery-search-heading" aria-controls="gallery-search">SEARCH</a>
            <div id="gallery-search" class="accordion-content row collapse" role="tabpanel" data-tab-content aria-labelledby="gallery-search-heading">
            	<!---
               		Application.generateAccordionGalleryFilters()
               		@params
               			intGalleryCategoryID int (opdation, default = 0): The categoryID for the gallery. If set to 0, returns filters by mapped websites
               			vchrMakeLabel varchar (optional, default = 'Make'): The label for the makes dropdown. if set to '', makes dropdown won't display
						vchrModelLabel (optional, default = 'Model'): The label for the models dropdown. if set to '', models dropdown won't display
						vchrBrandLabel (optional, default = 'Brands'): The label for the brands dropdown. if set to '', brands dropdown won't display
						vchrSearchLabel (optional, default = 'Search'): The label for the search field. if set to '', search field won't display
						vchrBrandTypeList (optional, default = '1'): List of brand types to display (1: wheels, 2: accessories, 3: tires, 4: private wheels, 5: private accessories, 6: private tires). Pass on as many as you need. 
               		
               ---->
				<cfoutput>#Application.generateAccordionGalleryFilters(Application.GALLERYID,'Make','Model','','Search')#</cfoutput>
            </div>
        </li>
    </ul>
 
</div>
 