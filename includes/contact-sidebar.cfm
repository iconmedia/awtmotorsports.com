<cfoutput>
    <div id="form-right" class="small-12 medium-4 columns padded">
        <div class="sidebar-content">
            <img src="#Application.SYSTEMIMAGEPATH#websites/logos/#application.settings.vchrSecondaryLogo#" alt="">
        </div>
        <div id="sidebar-location" class="sidebar-content">
            <ul>
                <li>
                    <a href="//www.google.com/maps/place/11350+Northwest+Fwy,+Houston,+TX+77092/@29.8218099,-95.4719909,17z/data=!3m1!4b1!4m5!3m4!1s0x8640c6ebd523d2f7:0x19a94a880c11dffb!8m2!3d29.8218053!4d-95.4697969" target="_blank">
                        <i class="fa fa-map-marker"></i>
                        <span><cfoutput>#Application.TITLE#<br />Performance Specialist<br/>#Application.settings.vchrPhysicalAddress# <br/> #Application.settings.vchrCity# #Application.settings.vchrState# #Application.settings.vchrPostalCode#</cfoutput></span>
                    </a>
                </li>
                <li>
                    <a href="tel:#Application.settings.vchrPhoneNumber#">
                        <i class="fa fa-phone"></i>
                        <span>TEL: #Application.settings.vchrPhoneNumber#</span>
                    </a>
                </li>
            </ul>
        </div>
        <div id="sidebar-hours" class="sidebar-content">
            <ul>
                <li><i class="fa fa-clock-o" aria-hidden="true"></i> #Application.settings.vchrHours#</li>
            </ul>
        </div>   
      
      
      
      
      
       <!---
        <h3 class="text-uppercase">#Application.TITLE# Inc.</h3>
        <ul class="no-bullet">
            <cfif application.settings.vchrPhysicalAddress neq ''>
				<li>
					<address>
						<cfoutput>
							#application.settings.vchrPhysicalAddress#
							<br>
							#application.settings.vchrCity#, #application.settings.vchrState# #application.settings.vchrPostalCode#
						</cfoutput>
					</address>
				</li>
			</cfif>
           	<cfif Application.settings.vchrPhoneNumber neq ''>
            	<li>#Application.settings.vchrPhoneNumber#</li>
			</cfif>
            <!---<li>(F) #Application.contactFax#</li>--->
            <cfif Application.settings.vchrRFQEmail neq ''>
            	<li>#Application.settings.vchrRFQEmail#</li>
			</cfif>
        </ul>
        <img src="/images/contact-logos.jpg">
        <cfif Application.settings.vchrGoogleMapsLink neq ''>
			<a href="#Application.settings.vchrGoogleMapsLink#" target="_blank">
				Click for directions
			</a>
		</cfif>
        <ul class="short padded menu simple">
           <!--- Application.generateSocialLinksList() 
                 Outputs a list of social media links. 
				@params 
					vchrOrder varchar (optinal, default = ''): Lets you select a custom order for the list. The defautl is facebook, twitter, instagram, youtube
					vchrLiClass (optional,default = ''): Lets you set a custom class for the li element
					vchrAClass (optional,default = ''): Lets you set a custom class for the a href element
					vchrIClass (optional,default = ''): Lets you set a custom class for the <i></i> element
					vchrTarget (optional, defautl = "_blank"): set to _self to have it open on the same tab
				NOTE: social links must be set in the admin for this to work. If they haven't been set, they won't show up. 
		   ---->
           <cfoutput>#application.generateSocialLinksList('intagram,youtube,facebook')#</cfoutput>
        </ul>
        --->
    </div>
</cfoutput>
