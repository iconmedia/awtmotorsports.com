<cfoutput query="GetWheelImage" group="intWheelID">
	<div class="images">
		<div class="main-image text-center">
			<div>
				<a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
					<img src="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#wheelName#" />
				</a>
			</div>
			<cfif GetAngles.recordcount gt 0>
				<cfloop query="GetAngles">
					<div>
						<a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
							<img src="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#GetWheelImage.wheelName#" />
						</a>
					</div>
				</cfloop>
			</cfif>
		</div>
		<cfif GetAngles.recordcount gt 0>
			<div class="row">
				<div class="alt-images medium-8 medium-offset-2 large-6 large-offset-3 text-center">
					<div>
						<img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#wheelName#" />
					</div>
					<cfloop query="GetAngles">
						<div>
							<img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#GetWheelImage.wheelName#" />
						</div>
					</cfloop>
				</div>
			</div>
		</cfif> 
	</div>
</cfoutput>