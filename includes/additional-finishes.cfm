<cfif !application.isVehicleSelected()>
	<div class="detail-finish row text-center short padded"></div>
		<div class="small-12 columns">
			<p class="black-font bold text-left">Additional Finishes</p>
		</div>
		<div class="medium-10 medium-offset-1 end columns">
			<div class="detail-thumbs row small-up-2 medium-up-3">
				<cfoutput query = "getRelatedWheelImages">
					<div class="column">
						<a data-id="#intWheelImageID#" data-lugs="#intLugs#" <cfif structKeyExists(session,"year") AND structKeyExists(session,"strMake") AND structKeyExists(session,"strModel") AND session.size gt 0 AND StructKeyExists(session, "size") AND StructKeyExists(partlist, Trim(getRelatedWheelImages.intWheelImageID))>data-part="#StructFind(partlist, Trim(getRelatedWheelImages.intWheelImageID))#"</cfif>>
							<img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#getRelatedWheelImages.vchrThumbnail#" alt="#vchrName# #finishName#" />
							<p><span class="finish-name">#finishName#</span></p>
							<p><strong class="lugs">#intLugs# Lug</strong></p>
						</a>
					</div>
				</cfoutput>
			</div>
		</div>
	</div>
</cfif>