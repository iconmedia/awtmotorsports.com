
        

    <!---<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--->
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.js"><\/script>')</script>
    <script src="/js/foundation.min.js"></script>
    <script src="/js/vendor/what-input.min.js"></script>
    <script src="/js/vendor/motion-ui.js"></script>
    <script>$(document).foundation();</script>
    <script src="/js/vendor/slick/slick.min.js"></script>
    <script src="/js/vendor/jquery.magnific-popup.min.js"></script>
    <script src="//iconfigurators.com/src/jquery.appguide2.js"></script>
    <!--- This requires that jquery already be loaded --->
    <div id="fitment-popup" class="white-popup mfp-hide">
		<h4 class="text-center">Fitment Lookup</h4>
		<cfinclude template="../../modules/views/app-guide.cfm">
	</div>
    
    
    <cfif attributes.page eq 'home'>
        <script src="/js/vendor/jquery-embedagram.js"></script>
        <script type="text/javascript" src="/js/vendor/instafeed.js"></script>
    </cfif>
    <cfif attributes.page eq 'gallery'>
        <script src="/js/vendor/jquery.infinitescroll.min.js"></script>
    </cfif>
    <cfif attributes.page eq 'inventory-results'>
        <script src="//listjs.com/no-cdn/list.js"></script>
    </cfif>
    <cfif attributes.page eq 'wheels'>
        <cfif StructKeyExists(url, "SizeID") AND url.SizeID gt 0 AND StructKeyExists(url, "fit") AND url.fit is "1" >
           <cfset getConfig = CreateObject("component", "models.config").getConfig(application.settings.intConfigID)>
           <script src="//ver1.iconfigurators.com/pop/fancybox/jquery.fancybox.js"></script>
           <script src="//ver1.iconfigurators.com/pop/src/popup-v2.cfm?key=<cfoutput>#getConfig.vchrKey#</cfoutput>"></script>
        </cfif>
    </cfif>
    <script src="/js/main.js"></script>
	<cfinclude template="../../modules/views/google-analytics.cfm">
	<!---<cfoutput>#application.footerScripts#</cfoutput>--->
	<!-- START DMG -->
 
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-98456538-1', 'auto');
      ga('send', 'pageview');

    </script>

    <!-- Google Code for Remarketing Tag -->
    <!--------------------------------------------------
    Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
    --------------------------------------------------->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 853520301;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/853520301/?guid=ON&amp;script=0"/>
    </div>
    </noscript>

    
    <script src="//scripts.iconnode.com/56557.js"></script>   

    <!-- END DMG -->
</body>
</html>
