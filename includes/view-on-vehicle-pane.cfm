<cfif structKeyExists(session,"year") AND structKeyExists(session,"strMake") AND structKeyExists(session,"strModel") AND StructKeyExists(session, "size") AND session.size gt 0>
	<cfoutput query="GetWheelImage" group="intWheelID">
		<div class="small-12 columns see-on-vehicle-detail">
			<cfset submodelColonIndex = findNoCase(":",session.submodel)>
			<cfif submodelColonIndex eq 0>
				<cfset intSubmodel = session.submodel>
				<cfset subOption = ''>
			<cfelse>
				<cfset intSubmodel = LEFT(session.submodel,submodelColonIndex - 1)>
				<cfset subOption = RIGHT(session.submodel,len(session.submodel) - submodelColonIndex)>
			</cfif>
			<cfif StructKeyExists(partlist, intWheelImageID)>
				<cfset partNumFits = StructFind(partlist, intWheelImageID)>
			<cfelseif StructCount(partlist) gt 0>
				<cfset partNumFits = StructFind(partlist, StructKeyArray(partlist)[1])>
			<cfelse>
				<cfset partNumFits = "">
			</cfif>
			<div id="see-on-vehicle-detail" <cfif StructCount(partlist) lte 0>style="display:none;"</cfif>
				class="short padded"
				data-year="#session.year#"
				data-make="#session.make#"
				data-model="#session.model#"
				data-submodel="#intSubmodel#"
				data-option="#subOption#"
				data-part="#partNumFits#"
				data-lugs="#Left(GetWheelParts.vchrBoltPattern, 1)#"
				data-config = "#Application.settings.intCONFIGID#">
				<div class="row collapse">
					<!---<div class="medium-6 columns">
						<h5>#session.year# #session.strmake# #session.strmodel#</h5>
					</div>--->
					<div class="medium-3 columns">
						<select name="colors" id="vehicle-colors"></select>
					</div>
					<div class="medium-3 end columns">
						<select name="body" id="vehicle-body" ></select>
					</div>
				</div>
				<a class="hover-item" href="/iconfigurator/##!year=#session.year#&make=#session.make#&model=#session.model#&submodel=#urlEncodedFormat(session.submodel)#&size=#replace(session.strsize, ' ', '%20', 'all')#&go=1&tab=wheels&page=1&body=0&color=&wheel=#intWheelID#&wheelimage=#intWheelImageID#">
					<div class="img-wrapper <cfif !StructKeyExists(partlist, intWheelImageID)>no-fitment</cfif>">
						<img src="" alt="" />
						<h6 class="error text-uppercase">Current finish/lug does not fit selected vehicle</h6>
					</div>
					<button class="black button small radius">See More Styles that Fit with the iConfigurator</button>
				</a>
			</div>
		</div>
	</cfoutput>
</cfif>