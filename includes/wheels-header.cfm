<cfparam name="brandArray" default="#arrayNew()#">
<cfparam name="finishArray" default="#arrayNew()#">
<cfparam name="lugArray" default="#arrayNew()#">
<cfparam name="diameterArray" default="#arrayNew()#">
<cfparam name="offsetArray" default="#arrayNew()#">
<cfquery
    name = "GetWheelBrands"
    datasource = "#Application.DSN#">
    SELECT DISTINCT 
        b.vchrName as BrandName
    FROM tbl_category c WITH (NOLOCK)
    INNER JOIN tbl_wheelCategory wc WITH (NOLOCK) ON wc.intCategoryId = c.intID
    INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wc.intWheelID
    INNER JOIN tbl_brand b WITH (NOLOCK) ON b.intID = w.intBrandID
    WHERE c.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#">
	AND b.vchrName <> 'rsr'
    <cfif attributes.page neq 'discontinued'>
        AND b.vchrName <> 'bazo'
    </cfif>
    ORDER BY b.vchrName
</cfquery>
<cfquery
    name = "GetWheelFinish"
    datasource = "#Application.DSN#">
    SELECT DISTINCT f.vchrShortName,
    c.intID
    FROM dbo.tbl_WheelImage wi
    INNER JOIN tbl_wheelcategory wc WITH (NOLOCK) ON wc.intWheelID = wi.intWheelID
    INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = wc.intCategoryID
    INNER JOIN tbl_finish f WITH (NOLOCK) ON f.intFinishID = wi.intFinishID
    AND c.intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.WHEELSID#"> 
    AND vchrShortName <> 'Custom'
    AND vchrShortName <> 'Grey'
    AND vchrShortName <> 'Silver'
    ORDER BY vchrShortName
</cfquery>
<cfquery
    name = "GetWheelSize"
    datasource = "#Application.DSN#">
    SELECT DISTINCT
    ws.decDiameter
    FROM dbo.tbl_WheelImage wi
    INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wi.intWheelID
    INNER JOIN tbl_wheelSpec ws WITH (NOLOCK) ON ws.intWHeelID = wi.intWheelID
    INNER JOIN tbl_brand b WITH (NOLOCK) ON b.intID = w.intBrandID
    WHERE ws.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#"> 
    AND b.vchrName <> 'rsr'
    ORDER BY decDiameter
</cfquery>
<cfquery
    name = "GetWheelBolt"
    datasource = "#Application.DSN#">
    SELECT DISTINCT
	wi.intLugs
    FROM dbo.tbl_WheelImage wi
    INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wi.intWheelID
    INNER JOIN tbl_wheelSpec ws WITH (NOLOCK) ON wi.intWHeelID = ws.intWheelID
    INNER JOIN tbl_brand b WITH (NOLOCK) ON b.intID = w.intBrandID
    WHERE ws.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#"> 
    AND b.vchrName <> 'rsr'
    ORDER BY intLugs
</cfquery>
<cfquery
    name = "GetWheelOffset"
    datasource = "#Application.DSN#">
    SELECT DISTINCT
    (SELECT TOP 1 TRY_CAST(ws.vchrOffset AS INT) FROM tbl_WheelImage) as intOffset
    FROM dbo.tbl_WheelImage wi 
    INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wi.intWheelID
    INNER JOIN tbl_wheelSpec ws WITH (NOLOCK) ON ws.intWHeelID = wi.intWheelID
    INNER JOIN tbl_brand b WITH (NOLOCK) ON b.intID = w.intBrandID
    WHERE ws.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#">
    AND b.vchrName <> 'rsr'
    ORDER BY intOffset ASC
</cfquery>

<div id="wheel-header" class="filter-header">
    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
        <li class="accordion-item">
            <a href="#wheel-search" role="tab" class="show-for-small-only accordion-title" id="gallery-search-heading" aria-controls="wheel-search">SEARCH</a>
            <div id="wheel-search" class="accordion-content row collapse" role="tabpanel" data-tab-content aria-labelledby="gallery-search-heading">
                <form action="/wheels/<cfif attributes.page eq 'discontinued'>discontinued.cfm</cfif>" method="get">
                    <div class="small-12 medium-2 columns">Filter By:</div>
                    <div class="small-12 medium-2 columns">
                        <div id="filter-brand" class="search-selector">
                            <label for="brand" id="brand"><span>Brand</span>
                                <ul>
                                    <cfoutput query="GetWheelBrands">
                                        <li><label><input type="checkbox" name="brand" value="#BrandName#" #checkIfChecked(brandArray, BrandName)#> #BrandName#</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="small-12 medium-2 columns">
                        <div id="filter-finish" class="search-selector">
                            <label for="finish" id="finish"><span>Finish</span>
                                <ul>
                                    <cfoutput query="GetWheelFinish" group="vchrShortName">
                                        <li><label><input type="checkbox" name="finish" value="#vchrShortName#" #checkIfChecked(finishArray, vchrShortName)#> #vchrShortName#</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <cfif attributes.page neq 'discontinued'><!--- hiding this column because there is no data as of 07/13/2016 --->
                        <div class="small-12 medium-2 columns">
                            <div id="filter-size" class="search-selector">
                                <label for="diameter" id="diameter"><span>Size</span>
                                    <ul>
                                        <cfoutput query="GetWheelSize">
                                            <li><label><input type="checkbox" name="diameter" value="#decDiameter#" #checkIfChecked(diameterArray, decDiameter)#> #decDiameter#"</label></li>
                                        </cfoutput> 
                                    </ul>
                                </label>
                            </div>
                        </div>
                    </cfif>
                    <div class="small-12 medium-2 columns end">
                        <div id="filter-bolt" class="search-selector">
                            <label for="bolt" id="bolt"><span>Bolt</span>
                                <ul>
                                    <cfoutput query="GetWheelBolt">
                                        <li><label><input type="checkbox" name="bolt" value="#intLugs#" #checkIfChecked(lugArray, intLugs)#> #intLugs# Lug</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <cfif attributes.page neq 'discontinued'>
                        <div class="small-12 medium-2 columns">
                            <div id="filter-offset" class="search-selector">
                                <label for="offset" id="offset"><span>Offset</span>
                                    <ul>
                                        <cfset areThereNegatives = 'false'>
                                        <cfset areThere0to20s = 'false'>
                                        <cfset arethere20to35s = false>
                                        <cfset areThere35andUps = 'false'>
                                        <cfoutput query="GetWheelOffset" group="intOffset">
                                           <cfset isNegative =  findNoCase('negative',url.offset)>
                                           <cfset is0to20 =  findNoCase('0to20',url.offset)>
                                           <cfset is20to35 =  findNoCase('20to35',url.offset)>
                                           <cfset is35andup =  findNoCase('35andup',url.offset)>
                                            <cfif intOffset lte 0 AND !areThereNegatives>
                                                <li><label><input type="checkbox" name="offset" value="negative" <cfif findNoCase('negative',url.offset) gt 0> checked</cfif>> Negative</label></li>
                                                <cfset areThereNegatives = 'true'>
                                            </cfif>
                                            <cfif intOffset gt 0 AND intOffset lte 20 AND !areThere0to20s>
                                                <li><label><input type="checkbox" name="offset" value="0to20" <cfif findNoCase('0to20',url.offset) gt 0> checked</cfif>> 0 to +20</label></li>
                                                <cfset areThere0to20s = true>
                                            </cfif>
                                            <cfif intOffset gt 20 AND intOffset lte 35 AND !arethere20to35s>
                                                <li><label><input type="checkbox" name="offset" value="20to35" <cfif findNoCase('20to35',url.offset) gt 0> checked</cfif>> +20 to +35</label></li>
                                                <cfset arethere20to35s = 'true'>
                                            </cfif>
                                            <cfif intOffset gt 35 AND areThereNegatives AND !areThere35andUps>
                                               <cfset areThere35andUps = 'true'>
                                                <li><label><input type="checkbox" name="offset" value="35andup" <cfif findNoCase('35andup',url.offset) gt 0> checked</cfif>> +35 and up</label></li>
                                            </cfif>
                                            
                                            <!---<li><label><input type="checkbox" name="offset" value="<cfif vchrOffset lte 0>#vchrOffset#</cfif>"> Negative</label></li>
                                            <li><label><input type="checkbox" name="offset" value="<cfif vchrOffset gt 0 AND vchrOffset lte 20>#vchrOffset#</cfif>"> 0 to +20</label></li>
                                            <li><label><input type="checkbox" name="offset" value="<cfif vchrOffset gt 20 AND vchrOffset lte 35>#vchrOffset#</cfif>"> +20 to +35</label></li>
                                            <li><label><input type="checkbox" name="offset" value="<cfif vchrOffset gt 35>#vchrOffset#</cfif>"> +35 and up</label></li>--->
                                        </cfoutput>
                                        
                                    </ul>
                                </label>
                            </div>
                        </div>
                    </cfif>            
                </form>
            </div>
        </li>
    </ul>
</div>
