<cfswitch expression="#thisTag.ExecutionMode#">
<cfcase value="start">

<cfinclude template = "../includes/header.cfm">

<!-- Header ------------------------------------------------>
<header class="fixed <cfif attributes.page neq 'home'>int-header</cfif>">
    <nav class="row top-bar" role="navigation" data-equalizer="header">
        <div class="top-bar-title" data-equalizer-watch="header" data-equalize-on="large">
            <span data-responsive-toggle="responsive-menu" data-hide-for="large" style="display:none;">
                <button class="menu-icon dark" type="button" data-toggle></button>
            </span>
            <div class="header-logo">
            	<a href="/">
            		<cfoutput>
            			<img src="#Application.SYSTEMIMAGEPATH#websites/logos/#application.settings.vchrPrimaryLogo#"><br />
            			Division of AMERICAN WHEEL AND TIRE
					</cfoutput>
            	</a>
            </div>
        </div>
        <div id="responsive-menu">
            <div class="top-bar-left" data-equalizer-watch="header">
              <div class="show-for-large header-contact text-right">
                    <cfoutput>
                    <h5>#Application.TITLE2#</h5>
                    <h2>#Application.settings.vchrPhoneNumber#</h2>
                </cfoutput>
                </div>
               <!---
               		Application.generateMenu()
               		@params
               			intMenuID int (optional, default = 0): The Menu ID for the menu
               			vchrType varchar (optional, default = ''): The menu Type. 'header', 'footer', 'sidebar', 'custom'
               			vchrTopUlClass varchar (optional, default = ''): The class(es) for the first UL. example: 'dropdown menu'
               			vchrTopUlAttr varchar (optional, default = ''): The attribute(s) for the first UL. example: 'data-dropdown-menu'
               			vchrSubUlClass varchar (optional, default= ''): The class(es) for the submenu uls. example: 'dropdown menu'
               			vchrSubUlAttr varchar (optional, default= ''): The attributes(s) for the submenu uls. example: 'data-dropdown-menu data-options="disableHover:true"'
               		Note: Classes for individual links are done in the admin. 
               		
               ---->
              <cfoutput>#application.generateMenu(0, 'header', 'dropdown menu', 'data-dropdown-menu', 'dropdown menu', 'data-dropdown-menu data-options="disableHover:true"')#</cfoutput>
<!---              <div id="search">
                <form action="/search-result.cfm" method="get" id="search-form">
                   <div class="row collapse">
                    <div class="small-4 large-3 columns">
                      <select name="searchType" required>
                        <option value="all" selected>All</option>
                        <option value="wheels">Wheels</option>
                        <option value="tires">Tires</option>
                        <option value="accessories">Accessories</option>
                      </select>
                    </div>
                    <div class="small-5 large-6 columns">
                      <input type="text" name="search" id="search-words" placeholder="Search.." value="">
                    </div>
                    <div class="small-3 large-3 columns">
                      <input type="submit" value="search" class="button expanded">
                    </div>
                  </div>
                </form>
              </div>--->
            </div>
            <!---<div class="top-bar-right show-for-large">
                <div class="nav-social">
                
                    <cfoutput>
                        <ul class="dropdown menu">
                            <li><a href="#Application.settings.vchrFacebookLink#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#Application.settings.vchrInstagramLink#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#Application.settings.vchrYoutubeLink#" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        </ul>
                    </cfoutput>
                    
            </div>--->
        </div>
    </nav>
</header>
<cfif attributes.page eq 'home'>
    <div id="content" class="hp site-content">
<cfelse>
    <div id="content" class="int site-content">
</cfif>
</cfcase>
<cfcase value="end">

<cfif attributes.page neq 'home' AND attributes.page neq 'contact'>
    <section class="int-specialist">
        <div class="row">
            <div class="column medium-12"><i class="fa fa-wrench fa-3x" aria-hidden="true"></i></div>
            <div class="column medium-12"><h4><cfoutput>#Application.TITLE#</cfoutput> AUTOMOTIVE Specialist - NEED maintenance or REPAIR? </h4></div>
            <div class="column medium-12"><a href="/contact/">schedule an appointment</a></div>
        </div>
    </section>
</cfif>
    
    </div>
<!-- Footer ------------------------------------------------>
<footer>
    <div class="footer">
       <section class="ft-support-links">
            <div class="row">
                <div class="column medium-3">
                    <cfoutput>
                        <div id="footer-logo">
                            <img src="#Application.SYSTEMIMAGEPATH#websites/logos/#application.settings.vchrPrimaryLogo#" alt="">
                        </div>
                        <div class="footer-desc">
                            <p>AWT Motorsports has technicians with over 50 years of combined service experience with expertise in exotic vehicles. We are your luxury car repair facility. We specialize in scheduled maintenance, repair and upgrades for Audi, BMW, Ferrari, Lexus, Mercedes, Porsche, all European vehicles and exotic makes and models.</p>
                        </div>
                        <div class="footer-social">
                            <ul>
                                <li><a href="#Application.settings.vchrFacebookLink#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#Application.settings.vchrInstagramLink#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#Application.settings.vchrYoutubeLink#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </cfoutput>
                </div>
                <div class="column medium-3">
                    <cfoutput>
                        <div id="location">
                            <h4>Location</h4>
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span><cfoutput>#Application.TITLE#<br />Performance Specialist<br/>#Application.settings.vchrPhysicalAddress# <br/> #Application.settings.vchrCity# #Application.settings.vchrState# #Application.settings.vchrPostalCode#</cfoutput></span>
                                </li>
                                <li>
                                    <a href="tel:#Application.settings.vchrPhoneNumber#">
                                        <i class="fa fa-phone"></i>
                                        <span>TEL: #Application.settings.vchrPhoneNumber#</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="hours">
                            <h4>Hours</h4>
                            <ul>
                                <li><i class="fa fa-clock-o" aria-hidden="true"></i> #Application.settings.vchrHours#</li>
                            </ul>
                        </div>   
                    </cfoutput> 
                </div>
                <div class="column medium-3">
                    <div class="ft-quick-links">
                        <h4>Quick Links</h4><cfoutput>#application.generateMenu(25, 'footer', '', '', '', '')#</cfoutput></div>
                    </div>
                <div class="column medium-3">
                    <h4>Newsletter</h4>
                    <p>Join our newsletter to get info about latest events and deals!</p>
                    <form action="/ajax/newsletter-submit.cfm" class="ajax-form newsletter-submit" data-title="Newsletter Signup">
                        <input type="email" name="email" value="" placeholder="Email Address" required>
                        <button type="submit" name="submit" class="button postfix">Join Now</button>
                    </form>
                    <div class="reveal" id="newsletterModal" data-reveal>
                      <p class="lead"></p>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                </div>   
            </div>
        </section>
        <section class="ft-bottom">
            <div class="row">
                <cfoutput>
                    <div class="column medium-12 text-center">
                        <p>&copy; #year(now())# #Application.TITLE#. ALL RIGHTS RESERVED.   </p>
                    </div>
                </cfoutput>
            </div>
        </section>
    </div>
</footer>

<cfinclude template = "../includes/footer.cfm">
</cfcase>
</cfswitch>
