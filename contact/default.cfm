<cfset captcha = application.makeRandomString()>
<cfset captchaHash = hash(captcha)>

<cfmodule template="/modules/layout.cfm"
	title="Contact - #Application.Title#"
	page="contact"
    keywords=""
	description=""
	metaTitle="">

<div id="contact">
    <cfoutput>
        <div class="parallax">
            <div class="img bg-top" style="background-image: url('/images/header-bg.jpg');"></div>
            <div class="row content-header">
                <div class="column medium-6 large-12">
                    <h1 class="text-uppercase white">CONTACT / SCHEDULE APPT.</h1>
                </div>
                <div class="column medium-6 show-for-medium-only">
                    <h5>#Application.TITLE2#</h5>
                    <h2>#Application.settings.vchrPhoneNumber#</h2>
                </div>
            </div>
            
        </div>
    </cfoutput>
    <div class="page-content row">
        <div class="small-12 columns text-center medium-10 medium-offset-1">
            <h1 class="text-uppercase">CONTACT us / Make AN Appointment</h1>
        </div>
        <div class="small-12 columns text-center medium-8 medium-offset-2">
            <p>
                Is your car due for regular maintenance? Do you have specific repair need?
Contact <cfoutput>#Application.TITLE#</cfoutput> to schedule an appointment. 
            </p>
        </div>
        <div id="form-left" class="small-12 medium-4 columns padded medium-offset-2">
            <form action="/ajax/connector.cfc?method=contactSubmit" method="post" id="contact-form">
				<cfif isDefined("form.captcha") AND hash(ucase(form.captcha)) neq Ucase(form.captchaHash)>
					<div class="error">Incorrect Captcha</div>
				</cfif>

				<div class="row">
                    <div class="small-12 columns">
                        <label for="name" class="font-bold">Contact*</label>
                        <input type="text" name="name" placeholder="Name" required />
                    </div>
                    <div class="small-12 columns">
                        <label for="phone" class="font-bold">Telephone*</label>
                        <input type="text" name="phone" placeholder="(555) 555-5555" required />
                    </div>
                    <div class="small-12 columns">
                        <label for="email" class="font-bold">Email*</label>
                        <input type="email" name="email" placeholder="xxx@xxx.xxx" required/>
                    </div>
                    <div class="small-12 columns">
                        <label for="vehicle" class="font-bold">Vehicle</label>
                        <input type="text" name="vehicle" placeholder="year, make, model" required />
                    </div>
                    <div class="small-12 columns">
                        <label for=subject class="font-bold">Subject</label>
                        <select name="subject">
                            <option>-- Select Service --</option>
                            <option value="Brake Inspection">Brake Inspection</option>
                            <option value="Car Not Starting">Car Not Starting</option>
                            <option value="Check Engine Light On">Check Engine Light On</option>
                            <option value="Courtesy Inspection/Trip Inspection">Courtesy Inspection/Trip Inspection</option>
                            <option value="Engine Oil &amp; Filter Service">Engine Oil &amp; Filter Service</option>
                            <option value="Factory Recommended Maintenance">Factory Recommended Maintenance</option>
                            <option value="Returning to Recommended Work">Returning to Recommended Work</option>
                            <option value="State Inspection">State Inspection</option>
                            <option value="Wheel Alignment Check">Wheel Alignment Check</option>
                            <option value="Tire Rotation">Tire Rotation</option>
                        </select>
                    </div>
                    <div class="small-12 columns">
                        <label for="message" class="font-bold">Message</label>
                        <textarea name="message" id="message" rows="6" placeholder="additional comments;
request an appointment time & date" required></textarea>
                    </div>
				</div>
				<div class="row">
                    <div class="small-12 columns">
                        <span id="check-captcha" class="primary"></span>
                        <div class="g-recaptcha" data-sitekey="<cfoutput>#Application.Settings.vchrRecaptchaSiteKey#</cfoutput>"></div>
                    </div>
	      		</div>
                <input type="submit" name="submit" value="SUBMIT" class="button check-captcha">
			</form>
            <p class="text-uppercase please-note">
                <span>Please note:</span> That the date and time you request may not be available. We will contact you to confirm your actual appointment details.
            </p>
        </div>
        <cfinclude template = "/includes/contact-sidebar.cfm">
    </div>
</div>

</cfmodule>
