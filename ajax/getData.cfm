<cfparam name="url.brand" default="0">
<cfparam name="url.wheel" default="0">
<cfparam name="url.finish" default="">
<cfparam name="url.diam" default="0.0">
<cfparam name="url.width" default="0.0">
<cfparam name="url.lug" default="">
<cfparam name="url.pcd" default="">
<cfparam name="url.bsmStart" default="0">
<cfparam name="url.bsmEnd" default="0">
<cfparam name="url.offSetStart" default="-180">
<cfparam name="url.offSetEnd" default="180">
<cfparam name="url.step" default="0">
<cfparam name="url.partnumber" default="">

<cfset strReturn = '' />

<cfswitch expression = "#url.step#" >

    <cfcase value="1">
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_Wheels_By_Brand-Brand]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID# " dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand# " dbvarname="@intBrandID">
            <cfprocresult name="getWheels" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getWheels">
	        <cfset strReturn  =  strReturn &'<option value="#intWheelid#">#Trim(vchrName)#&nbsp;&nbsp;&nbsp;</option>' />
	    </cfoutput>
	</cfcase>

    <cfcase value="2" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelFinish_By_Brand_Wheel-Finish]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID# " dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand# " dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel# " dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocresult name="getFinish" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getFinish">
            <cfset strReturn  =  strReturn &'<option value="#Trim(vchrFinishName)#">#Trim(vchrFinishName)#</option>' />
        </cfoutput>
	</cfcase>

    <cfcase value="3" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelDiam_By_Brand_Wheel_Finish-Diam]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID# " dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand#" dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel#" dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal"  scale="1" value="#url.Diam#" dbvarname="@DecDiameter">
            <cfprocresult name="getDiam" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getDiam">
	        <cfset strReturn  =  strReturn &'<option value="#decDiameter#">#Trim(decDiameter)#</option>' />
	    </cfoutput>
	</cfcase>

    <cfcase value="4" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelWidth_By_Brand_Wheel_Finish_Diam-Width]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID# " dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand#" dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel#" dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocparam  type="in"  cfsqltype="cf_sql_decimal" scale="1" value="#url.Diam#" dbvarname="@DecDiameter">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal"  scale="2" value="#url.Width#" dbvarname="@DecWidth">
            <cfprocresult name="getWidth" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getWidth">
	        <cfset strReturn  =  strReturn &'<option value="#decWidth#">#Trim(decWidth)#</option>' />
	    </cfoutput>
    </cfcase>

    <cfcase value="5" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelBoltPattern_By_Brand_Wheel_Finish_Diam_Width-Lugs]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#" dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand#" dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel#" dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Diam#"  scale="1" dbvarname="@DecDiameter">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Width#"  scale="2" dbvarname="@DecWidth">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.lug#" dbvarname="@vchrLug">
            <cfprocresult name="getLugs" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getLugs">
	        <cfset strReturn  =  strReturn &'<option value="#Trim(lug)#">#Trim(lug)#</option>' />
	    </cfoutput>
	</cfcase>

    <cfcase value="6" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelBoltPattern_By_Brand_Wheel_Finish_Diam_Width_Lugs-PCD]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#" dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand#" dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel#" dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Diam#"  scale="1" dbvarname="@DecDiameter">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Width#"  scale="2" dbvarname="@DecWidth">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.lug#" dbvarname="@vchrLug">
            <cfprocresult name="getPCDS" >
        </cfstoredproc>

        <cfset strReturn = '<option value="">All</option>' />
	    <cfoutput QUERY="getPCDS">
	        <cfset strReturn  =  strReturn &'<option value="#Trim(boltPattern)#">#Trim(boltPattern)#</option>' />
	    </cfoutput>
	</cfcase>

    <cfcase value="7" >
        <cfstoredproc PROCEDURE="Inventory.dbo.[sp_get_ULT_Ajax_WheelOffSet_By_Brand_Wheel_Finish_Diam_Width_Bolt-Offset]"
            datasource="#Application.DSN#">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID# " dbvarname="@intAccountID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.brand# " dbvarname="@intBrandID">
            <cfprocparam  type="in" cfsqltype="cf_sql_integer" value="#url.Wheel#" dbvarname="@intWheelID">
            <cfprocparam  type="in" cfsqltype="cf_sql_varchar" value="#url.Finish#" dbvarname="@vchrFinish">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Diam#"  scale="1" dbvarname="@DecDiameter">
            <cfprocparam  type="in" cfsqltype="cf_sql_decimal" value="#url.Width#"  scale="2" dbvarname="@DecWidth">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.lug#" dbvarname="@vchrLug">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.PCD#" dbvarname="@vchrBCD">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.OffSetStart#" dbvarname="@vchrOffSetStart">
            <cfprocparam  type="in"  cfsqltype="cf_sql_varchar" value="#url.OffSetEnd#" dbvarname="@vchrOffSetEnd">
            <cfprocresult name="getOffSet" >
        </cfstoredproc>

	    <cfoutput QUERY="getOffSet">
	        <cfset strReturn  =  strReturn & "#vchrOffSetStart#,#vchrOffSetEnd#" />
	    </cfoutput>
    </cfcase>

</cfswitch>

<cfoutput>#strReturn#</cfoutput>
