<cfcomponent extends="controllers.connector" hint="This is a shell to do all ajax functions in. You can add more for individual sites if you need">
	<cffunction name="init">
		<cfreturn this>
	</cffunction>
	<cffunction name="contactSubmit" access="remote" hint="This is a very basic contact submit. We will add more features as needed">
		<cfcontent type="text/html">
		<cfif isdefined("form.submit")>
            <cfset x = getHTTPRequestData()>
            <cfset clientIP = ''>
            <cfif structKeyExists(x.headers,"X-Forwarded-For")>
				<cfset clientIP = x.headers["X-Forwarded-For"] >
            </cfif>
            <cfset recaptcha = FORM["g-recaptcha-response"] >
			<cfif len(recaptcha)>
            	<cfset recaptchaUrl = "https://www.google.com/recaptcha/api/siteverify">
				<cfif Application.Settings.vchrRecaptchaSecretKey neq ''>
					<cfset secret = "#Application.Settings.vchrRecaptchaSecretKey#">
				<cfelse>
					<cfset secret = "6LfPDAwUAAAAAEO55xYyapBFc2vGrEP31t3HWHWx">
				</cfif>
            	<cfset ipaddr = CGI.REMOTE_ADDR>
            	<cfset request_url = recaptchaUrl & '?secret=' & secret & '&response=' &recaptcha & '&remoteip' & ipaddr>

            	<cfhttp url="#request_url#" method="get" timeout="10">

           		<cfset response = deserializeJSON(cfhttp.filecontent)>
            	<cfif response.success eq "YES">
					<cfmail
						from="rfq@icon-media.com"
						to="#Application.settings.vchrRFQEmail#"
						replyto="#form.email#"
						subject="New Submission - #Application.TITLE# Contact Form"
						type="html">
						Name: #form.name#<br />
						Email Address: #form.email#<br />
						<cfif structKeyExists(form, "phone")>
							Phone: #form.Phone# <br />
						</cfif>
						<cfif structKeyExists(form, "subject")>
							Subject: #form.subject# <br />
						</cfif>
						<cfif structKeyExists(form, "vehicle")>
							Vehicle: #form.vehicle# <br />
						</cfif>
						Submitted the following:<br />
						#form.message#
					</cfmail>
					<cfif structKeyExists(form, "redirectTo")>
						<cflocation url="/#form.redirectTo#">
					<cfelse>
						<cflocation url="/thank-you.cfm">
					</cfif>
				<cfelse>
					<cflocation url="#cgi.http_referer#?error=captcha did not match">
				</cfif>
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent> 