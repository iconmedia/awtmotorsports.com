<cfif IsValid("email", form.email)>
    <cfquery
        name = "CheckEmail"
        datasource = "#Application.DSN#">
        SELECT vchrEmail
        FROM tbl_emailsubscription
        WHERE intCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.WEBSITEID#">
          AND vchrEmail = <cfqueryparam value = "#form.email#" CFSQLType = "cf_sql_varchar" maxlength = "75">
    </cfquery>

    <cfif CheckEmail.recordcount eq 0>
        <cfquery
            name = "InsertEmail"
            datasource = "#Application.DSN#">
            INSERT INTO tbl_emailsubscription (intAccountID, vchrEmail, intCategoryId)
            VALUES (<cfqueryparam cfsqltype="cf_sql_integer" value="#Application.settings.intAccountID#">, <cfqueryparam value = "#form.email#" CFSQLType = "cf_sql_varchar" maxlength = "75">, <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.WEBSITEID#">)
        </cfquery>
        Email address successfully added to newsletter.
    <cfelse>
        This email address has already been added to our subscriber list. Thank you for your continued patronage.
    </cfif>
<cfelse>
    Invalid Email format. Please correct and try again.
</cfif>
