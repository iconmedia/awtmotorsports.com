<cfparam default="" name="url.id" />

<cfquery name="GetWheelImage"
    datasource="#Application.DSN#" >
    SELECT DISTINCT wi.intWheelImageID
        ,wi.intWheelID
        ,wi.vchrImage
        ,wi.vchrThumbnail
        ,wi.intFinishID
        ,wi.intLugs
        ,wi.vchrImageLarge
        ,wi.vchrImageXLarge
        ,w.vchrName AS wheelName
    FROM tbl_WheelImage wi WITH (NOLOCK)
    INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wi.intWheelID
    INNER JOIN tbl_wheelcategory wc WITH (NOLOCK) ON wc.intWheelID = w.intWheelID
    INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = wc.intCategoryId
    WHERE wi.intWheelImageID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.id#">
      AND c.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.Settings.intAccountID#">
      AND wi.blnDefault = 1
</cfquery>

<cfquery
	name="GetAngles"
    datasource="#Application.DSN#">
	SELECT wi.vchrImageLarge
        ,wi.vchrThumbnail
        ,wi.vchrImage
		,wi.vchrImageXLarge
		,wi.intWheelViewID
	FROM tbl_wheelimage wi WITH(NOLOCK)
    INNER JOIN tbl_wheel w WITH(NOLOCK) ON w.intWheelID = wi.intWheelID
	WHERE wi.intWheelViewID IN (1,2,3,4)
        AND w.intWheelID = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetWheelImage.intWheelID#">
        AND wi.intWheelImageID != <cfqueryparam cfsqltype="cf_sql_integer" value="#GetWheelImage.intWheelImageID#">
        AND wi.intFinishID = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetWheelImage.intFinishID#">
        AND wi.intLugs = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetWheelImage.intLugs#">
        AND w.blnActive = 1
	ORDER BY wi.blnDefault DESC, wi.blnDefaultFinish DESC, wi.intWheelViewID DESC, wi.intWheelImageID
</cfquery>

<cfoutput query="GetWheelImage" group="intWheelID">
    <div class="main-image text-center">
        <div>
            <a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
                <img data-lazy="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#wheelName#" />
            </a>
        </div>
        <cfif GetAngles.recordcount gt 0>
            <cfloop query="GetAngles">
                <div>
                    <a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
                        <img data-lazy="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#GetWheelImage.wheelName#" />
                    </a>
                </div>
            </cfloop>
        </cfif>
    </div>
    <cfif GetAngles.recordcount gt 0>
        <div class="row">
            <div class="alt-images medium-8 medium-offset-2 large-6 large-offset-3 text-center">
                <div>
                    <img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#wheelName#" />
                </div>
                <cfloop query="GetAngles">
                    <div>
                        <img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#GetWheelImage.wheelName#" />
                    </div>
                </cfloop>
            </div>
        </div>
    </cfif>
</cfoutput>
