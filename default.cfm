<!----
    createObject("component", "models.gallery").getGalleryByCategory()
    @params
        @intGalleryCategoryID int (optional, default = 0) : The category ID to search by
        @intGalleryID int (optional, default = 0) : If you only want to show a single gallery. Meant to be used on gallery-vehicles.cfm
        @blnShowExclusiveResults boolean(optional, default= false) : If set to true, all words in the search have to match. So, if you are searching for "Jeep Wrangler", it would not show results for a Jeep Liberty
---->
<cfset getGallery = createObject("component", "models.gallery").getGalleryByCategory(5618)>


<cfquery name="getSlides" datasource="#application.DSN#">
			SELECT si.vchrImage
                ,si.vchrDescription
                ,si.vchrSeo
                ,si.vchrLink
                ,si.vchrThumbnail
                ,s.blnAnchor
                ,s.intTimeout
                ,s.intSpeed
            FROM tbl_slideshow_image si
            INNER JOIN tbl_slideshow s ON s.intSlideID = si.intSlideID
            WHERE si.intSlideID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.SLIDERID#" />
            ORDER BY si.intSort
		</cfquery>

<cfmodule template="modules/layout.cfm"
	title="AWT Motorsports - Houston"
	page="home"
    keywords="custom wheels,suspension lifts,import tuning,car restoration,performance upgrades for domestics and imports,exhaust,superchargers,turbos,bmw,mercedes,lexus,ferrari,porsche,houston,texas"
	description="AWT Specializes in Wheels, Tires, Suspension Lifts, Maintenance and more on Most Vehicle Brands, Both Import and Domestic, Luxury and Exotic."
	metaTitle="">
<!-- Hero -------------------------------------->
<section class="hp-hero">
    <div class="row">
        <div class="parallax">
            <!---
               		Application.generateSlider()
               		@params
               			intSliderID int (required, default = 0): The Slider ID for the Slider
               ---->
			<!---<cfoutput>#application.generateSlider(272)#</cfoutput>--->

            <!---<div class="hero-text text-center column small-12">
                <h1><cfoutput>#Application.TITLE2#</cfoutput></h1>
                <h5 class="side-bars pulse-fade text-center white font-italic">Scroll for more</h5>
                <div class="scroll-arrow"><i class="orange fa fa-arrow-circle-down fa-3x" aria-hidden="true"></i></div>
            </div>--->
            
            
            <div class="hp-slider">
                <cfoutput query="getSlides">
                    <div>
                        <div class="slider-image" data-thumb="#Application.SystemImagePath#slides/#vchrImage#"><img src="#Application.SystemImagePath#slides/#vchrImage#" alt="#vchrSeo#"></div>
                        <!---<div class="slider-caption">#vchrDescription#</div>--->
                        <div class="slider-caption">
                            <div class="slider-row">
                                <div class="column large-3 show-for-large">
                                    <img src="#Application.SystemImagePath#slidethumbs/#vchrThumbnail#" />
                                </div>
                                <div class="column large-9">
                                    #vchrDescription#
                                </div>
                            </div>
                        </div>
                        <!---<div class="slider-caption hide-for-large">
                            <a href="#vchrLink#">#vchrSeo#</a>
                        </div>--->
                        <!---<div class="slider-caption">
                            <div class="row">
                                <div class="column medium-3">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                </div>
                                <div class="column medium-9">
                                    <h1>YOUR LUXURY CAR REPAIR &amp; MAINTENANCE FACILITY</h1>
                                    <h4>scheduled MAINTENANCE, TIRE ROTATION,<br /><span class="orange">
                                        TUNE-UPS, Dealership Alternative</span>
                                    </h4>
                                    <a href="/contact/" class="slider-btn">Schedule appointment</a>
                                    <a href="/" class="slider-btn slider-btn-secondary">Schedule appointment</a>
                                </div>
                            </div>
                        </div>--->
                    </div> 
                </cfoutput>
            </div>
            <div id="slider-arrows"></div>
            <div class="slick-thumbs">
                <ul>
                    <cfoutput query="getSlides">
                        <li><img src="#Application.SystemImagePath#slides/#vchrImage#" /><p>#vchrSEO#</p></li>
                    </cfoutput>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--- Custom Vehicle --->
<!---
<section class="hp-custom-vehicle">
    <div class="row full-width">
        <cfoutput>
            <div class="column medium-6"><a href="http://awtjeepedition.com/" target="_blank"><img src="/images/custom-jeeps.jpg" alt="#Application.TITLE# - Custom Jeeps"/></a></div>
            <div class="column medium-6"><a href="http://awttruckedition.com/" target="_blank"><img src="/images/custom-trucks.jpg" alt="#Application.TITLE# - Custom Trucks"/></a></div>
        </cfoutput>
    </div>
</section>
--->

<!--- specialization --->
<section class="hp-specialization">
    <div class="row">
        <div class="column medium-12">
            <h2>We specialize in scheduled maintenance, repair and <span class="orange">upgrades for all European vehicles.</span></h2>
            <div class="lowerlink hide-for-small-only">
                <div>performace upgrades</div><div><i class="fa fa-angle-double-down" aria-hidden="true"></i></div><div>domestic and imports</div>
            </div>
        </div>
    </div>
</section>


<!--- new --->
    <div class="parallax hp-new-parallax">
        <div class="img" style="background-image: url(/images/bg-what-new.jpg);"></div>
<section class="hp-new">
    
    <div class="row">
        <div class="column medium-12">
            <h2>what&#39;s new at <cfoutput>#Application.TITLE#</cfoutput></h2>
        </div>
    </div>


    <div class="row hp-new-content">
        <div class="column medium-6 large-7">
            <img class="animate" src="/images/awt-motorsports-service-repair-facility.jpg" />
        </div>
        <div class="column medium-6 large-5">
            <ul>
                <li class="animate"><h4>newly designed<br /><span class="orange">service &amp; repair facility</span></h4>
                <p>Our technicians have over 50 years of combined service experience and expertise in exotic vehicles. We are your luxury car repair facility.</p></li>
                <li class="animate"><h4>full-service center<br /><span class="orange">precision maintenance</span></h4>
                <p>Looking for simple scheduled maintenance for your import, exotic or domestic pride and joy?</p></li>
            </ul>
        </div>
    </div>
    <div class="row hide-for-small-only">
        <div class="column medium-12">
            <div class="lowerlink">
                <div>need auto repair or service</div><div><i class="fa fa-calendar-check-o animate" aria-hidden="true"></i></div><div>schedule an appointment</div>
            </div> 
        </div>
    </div>

</section>
    </div>


<!--- why section --->
<!---<section class="hp-explanation">
    <div class="row">
        <div class="column medium-12">
            <h2 class="white">WHY YOU SHOULD CHOOSE <span class="orange"><cfoutput>#Application.TITLE#</cfoutput></span></h2>
        </div>
        <div class="column medium-12"><img src="/images/hp-explanation.png" /></div>
        <div class="column medium-12">
            <a href="">who we are</a>
        </div>
    </div>
</section>--->

<!--- why section --->
<section class="hp-explanation hide-for-small-only">
    <div class="row">
        <div class="column medium-12">
            <h2 class="white">WHY YOU SHOULD CHOOSE <span class="orange"><cfoutput>#Application.TITLE#</cfoutput></span></h2>
        </div>
        <div class="column small-3 hp-explanation-list hp-left-list">
            <ul>
                <li><i class="fa fa-wrench animate" aria-hidden="true"></i> KNOWLEDGEABLE Experienced techs &#8220;Your one stop facility&#8221;</li>
                <li><i class="fa fa-arrows animate" aria-hidden="true"></i> OEM TOOLs, scanners and specialty equipment</li>
                <li><i class="fa fa-cogs animate" aria-hidden="true"></i> FULL RACE SETUP / EXPERIENCE AS WELL &#8220;WHERE EVERY DETAIL MATTERS&#8221;</li>
            </ul>
        </div>
        <div class="column small-6 animate"><img src="/images/hp-engine.png" /></div>
        <div class="column small-3 hp-explanation-list hp-right-list">
            <ul>
                <li><i class="fa fa-tachometer animate" aria-hidden="true"></i> RESTORATION EXPERTS</li>
                <li><i class="fa fa-cubes animate" aria-hidden="true"></i> SCHEDULED MAINTENANCE FOR ALL EXOTICS TO THE FINEST DOMESTICS</li>
                <li><i class="fa fa-signal animate" aria-hidden="true"></i> FROM MILD TO WILDEST. WE HAVE DONE IT.</li>
            </ul>
        </div>
        <div class="column medium-12">
            <a href="/p/8/about-us/">who we are</a>
        </div>
    </div>
</section>


<!--- about us --->
<section class="hp-about">
    <div class="row">
        <div class="column medium-12">
            <h3>We install the <span class="orange">top aftermarket brands</span></h3>
        </div>
    </div>
    <div class="hp-about-cta">
        <div class="row" data-equalizer>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-01.jpg" />
                </div>
                <h4><a href="/p/4/suspension-tuning/">Suspension Tuning</a></h4>
                <p>BC, Bilstein, BMR, Hotchkiss, KW, QA-1, Tein, Whiteline and more!</p>
            </div>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-02.jpg" />
                </div>
                <h4><a href="/p/6/wheels/">Wheels</a> &amp; <a href="/p/5/tires/">Tires</a></h4>
                <p>ADV-1, Avant, HRE, Lexani, LF Forged, Raceline Billet, Work, BF Goodrich, Continental, Michelin, Nitto, Pirelli, Toyo, Yokohoma and more!</p>
            </div>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-03.jpg" />
                </div>
                <h4><a href="/p/7/performance-tuning/">Performance</a></h4>
                <p>AMS, APR, Akropovich, Capristo, DTE, Fabspeed, Mansory, Novitech Rosso, Promotive, RennTech and more!</p>
            </div>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-04.jpg" />
                </div>
                <h4><a href="/p/2/classic-car-restoration-service/">Restorations</a></h4>
                <p>AWT Motorsports has restored some of the most rare vehicles. Some within Houston, others delivered from out of state. Consult with our experts for your build.</p>
            </div>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-05.jpg" />
                </div>
                <h4><a href="/p/3/service-repair/">Service</a></h4>
                <p>At AWT Motorsports, we can service any type of vehicle. From BMW, Mercedes Benz, Audi, and Porsche German Imports to exotics like Aston Martin, Bentley, and even the Italian Ferrari and Lamborghini models.</p>
            </div>
            <div class="column medium-4" data-equalizer-watch>
                <div class="hp-about-cta-icon">
                   <img src="/images/hp-about-06.jpg" />
                </div>
                <h4><a href="http://www.awtjeepedition.com/" target="_blank">Custom Jeeps</a></h4>
                <p>AWT is known for their custom built Jeeps and Trucks. With our volume pricing, AWT builds some of the best custom vehicles on the road. Inquire Today</p>
            </div>
        </div>  
    </div>
    <div class="row collapse hp-about-icon hide-for-small-only">
        <div class="column animate"><img src="/images/hp/adv1.jpg" /></div>
        <div class="column animate"><img src="/images/hp/bilstein.jpg" /></div>
        <div class="column animate"><img src="/images/hp/dte.jpg" /></div>
        <div class="column animate"><img src="/images/hp/hotchkiss.jpg" /></div>
        <div class="column animate"><img src="/images/hp/hre.jpg" /></div>
        <div class="column animate"><img src="/images/hp/lexani.jpg" /></div>
        <div class="column animate"><img src="/images/hp/yokohoma.jpg" /></div>
    </div>
    <div class="row small-up-3 hp-about-icon-mobile show-for-small-only">
        <div class="column animate"><img src="/images/hp/adv1.jpg" /></div>
        <div class="column animate"><img src="/images/hp/bilstein.jpg" /></div>
        <div class="column animate"><img src="/images/hp/dte.jpg" /></div>
        <div class="column animate"><img src="/images/hp/hotchkiss.jpg" /></div>
        <div class="column animate"><img src="/images/hp/hre.jpg" /></div>
        <div class="column animate"><img src="/images/hp/lexani.jpg" /></div>
        <div class="column animate"><img src="/images/hp/yokohoma.jpg" /></div>
    </div>
</section>

<!--- instagram --->
<section id="instagram">
    <div id="instafeed" class="row expanded collapse small-up-2 medium-up-3 large-up-5"></div>
</section>
<!---
<section class="hp-video">
         <!---
            application.generateYoutubePlaylist()
            @params
                smallColumn (optional, default = 1): # of videos to display in a row on small screens
                mediumColumn (optional, default = 2): # of videos to display in a row on medium and up screens
         --->
    <cfoutput>#application.generateYoutubePlaylist()#</cfoutput>
</section>
--->
</cfmodule>
